var filtered_rewards = local_storage_load_list('filtered-rewards');


var bind_filter_controls = function() {
    // HandlebarsJS template context cannot use naked list variable
    var alerts_context = [];
    for (var i=0; i<filtered_rewards.length; i++) {
        alerts_context.push({
            reward: filtered_rewards[i]
        });
    }

    var restore_page = $('#restore-page');
    var restore_page_content = $('#restore-page div[data-role=content]');
    var source = $('#restore-alerts-template').html();
    var template = Handlebars.compile(source);
    var html = template({
        'alerts': alerts_context
    });
    restore_page_content.html(html);

    if (filtered_rewards.length == 0) {
        restore_page_content.html('<p>No Filters</p>');
        return;
    }

    // Restore alert button
    restore_page.find('.restore-alert').on('tap', function(event) {
        var reward = $(this).text().trim();

        for (var i=(filtered_rewards.length-1); i>-1; i--) {
            if (filtered_rewards[i] == reward) {
                filtered_rewards.splice(i, 1);
                local_storage_save_list('filtered-rewards', filtered_rewards);
            }
        }

        bind_filter_controls();
    });

    restore_page_content.trigger('create');
}


var bind_navigation_controls = function() {
    $('#restore-alerts').on('tap', function() {
        bind_filter_controls();
    });

    $('#back-to-alerts').on('tap', function() {
        render_mission_list();
    });
}
