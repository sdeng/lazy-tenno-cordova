// TODO: Find out if this needs to be in global namespace
function display_notification(event) {
    // TODO: Fetch missions from server and display anything current not already in datamass
    if (event.alert) {
        console.log(event.alert);
    }

    if (event.sound) {
        var sound = new Media(event.sound);
        sound.play();
    }

    if (event.badge) {
        window.plugins.pushNotification.setApplicationIconBadgeNumber(
            function(result) {
                console.log('Success: ' + result);
            },
            function(error) {
                console.log('Error: ' + error);
            },
        event.badge);
    }
}


(function() {
    var registration_success = function(token) {
        socket.emit('register', {
            token: token.toString(16)
        });
    }


    var registration_error = function(error) {
        console.log('Registration error: ' + error.toString());
    }


    var register_device = function() {
        // TODO: Detect device type (iOS/Android/WP7/WP8)
        window.plugins.pushNotification.register(registration_success, registration_error, {
            badge: 'true',
            sound: 'true',
            alert: 'true',
            ecb: 'display_notification'
        });
    }


    document.addEventListener('deviceready', function() {
        if (window.location.protocol == 'http:') {
            console.log('Cordova not detected. Skipping push notification.');
            return;
        }

        register_device();
    }, false);
}());
