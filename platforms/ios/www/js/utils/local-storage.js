var local_storage_load_list = function(key) {
    var local_storage_value = window.localStorage.getItem(key);

    if ((local_storage_value == null) || (local_storage_value == "")) {
        return [];
    }

    return local_storage_value.split(',');
}


var local_storage_save_list = function(key, value_list) {
    var value = value_list.join(',');
    window.localStorage.setItem(key, value);
}
