var missions = [];


var render_mission_list = function() {
    var alert_list = $('#alert-list').empty();
    var rendered_missions = 0;

    for (var i=0; i<missions.length; i++) {
        var valid_mission = render_mission(missions[i]);

        if (valid_mission) {
            rendered_missions++;
        }
    }

    if (rendered_missions == 0) {
        alert_list.append('<p>No Alerts</p>');
    }

    // Apply jQuery Mobile styles on fresh elements
    alert_list.trigger('create');
}


var render_mission = function(mission) {
    // Expired missions
    var milliseconds_remaining = mission.expiration - Date.now();
    if (milliseconds_remaining < 0) {
        return false;
    }

    // Skip filtered rewards
    var reward = mission.reward == null ? mission.credits : mission.reward;
    for (var i=0; i<filtered_rewards.length; i++) {
        if (filtered_rewards[i] == reward) {
            return false;
        }
    }

    var source = $('#mission-template').html();
    var template = Handlebars.compile(source);
    var html = template({
        'mission': mission,
        'reward': reward,
        'minutes_remaining': Math.round(milliseconds_remaining / (60 * 1000))
    });

    $('#alert-list').append(html);
    bind_alert_filter(reward);

    return true;
}


var bind_alert_filter = function(reward) {
    var mission = $('#alert-list').children().last();
    var filter_button = mission.find('a.filter-alert');

    mission.on('swiperight', function() {
        filter_button.toggleClass('hidden');
    });

    filter_button.on('tap', function() {
        filtered_rewards.push(reward);
        local_storage_save_list('filtered-rewards', filtered_rewards);

        render_mission_list();
    });
}



var remove_credit_missions = function(data) {
    for (var i=(data.length-1); i>-1; i--) {
        if (data[i].reward == null) {
            data.splice(i, 1);
        }
    }

    return data;
}
