// Connect to Bossy Lotus
var socket = io.connect('http://samdeng.com:7000');


$('document').ready(function() {
    // Real-time mission updates
    socket.on('mission', function(data) {
        missions = remove_credit_missions(data);
        missions = missions.reverse();
        render_mission_list();
    });

    bind_navigation_controls();
    bind_filter_controls();

    // Frequently re-render missions client-side to update time remaining
    setInterval(function() {
        render_mission_list();
    }, 30000);
});
